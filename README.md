# Flask API Chat with Web Client project #
## What is this? ##
Test task for hiring.
## What it does? ##
Just a small chat web application with user authentication.
## How to run? ##
* Create a clean virtual environment.
* Go to main application's directory and run pip install -r requirements.txt
* Copy config_template.py to config.py and fill in MongoDB credentials:
    DB_USER = ''
    DB_USER_PASSWORD = ''
    DB_NAME = ''
* Launch application by running command "python runserver.py"
* Open http://your_server_address:5000 in your browser.
* Register a new user & enjoy.

## Prerequisites ##
That application uses RabbitMQ queue so you need to install that broker
as described on [https://www.rabbitmq.com/install-debian.html](https://www.rabbitmq.com/install-debian.html) page
in **"Using rabbitmq.com APT Repository"** section.
