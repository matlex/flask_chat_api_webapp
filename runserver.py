from chat_api import app, socketio
from config import DEBUG_MODE


if __name__ == "__main__":
    socketio.run(app=app, host='0.0.0.0', debug=DEBUG_MODE)
